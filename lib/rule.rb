module Rule
end

class Rule::R001
  LABEL = '10% off any order of 60GBP or more'

  def apply(items, requested_kind)
    total = items.inject(0) do |sum, item|
      sum + item.value
    end

    if total >= 6000 && requested_kind == :per_order
      items += [Discount.new(LABEL, -(total / 10))]
    else
      items
    end
  end
end

class Rule::R002
  LABEL = 'Buy 2 or more travel card and the value drops to 8.50'
  
  def apply(items, requested_kind)
    if requested_kind == :per_item && applicable_item_count(items) >= 2
      apply_discount(items)
    else
      items
    end
  end

  def apply_discount(items)
    items.map do |i|
      i.code == '001' ? Discounted.new(i, 850) : i
    end
  end

  def applicable_item_count(items)
    items.count { |p| p.code == '001' }
  end
end
