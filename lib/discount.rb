class Discounted < SimpleDelegator
  attr_reader :value

  def initialize(product, new_value) # add rule, for extra niceness.
    __setobj__(product)
    @value = new_value
  end
end

class Discount < Struct.new(:label, :value)
  def code
    'DISCOUNT'
  end
end

