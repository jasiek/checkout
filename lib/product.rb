class Product < Struct.new(:code, :label, :value)
  def to_s
    [code, label, value].join(' ')
  end
end

Product::P001 = Product.new('001', 'Travel Card Holder', 925)
Product::P002 = Product.new('002', 'Personalised cufflinks', 4500)
Product::P003 = Product.new('003', 'Kids T-shirt', 1995)

