class Checkout
  def initialize(rules)
    @rules = rules
    @items = []
  end

  def scan(product)
    @items << product
  end

  def items
    [:per_item, :per_order].inject(@items) do |items, kind|
      @rules.inject(items) do |items, rule|
        rule.new.apply(items, kind)
      end
    end
  end

  def total
    items.inject(0) do |sum, item|
      sum + item.value
    end
  end
end
