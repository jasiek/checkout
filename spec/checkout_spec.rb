require 'helper'

RSpec.describe 'test' do
  let :rules do
    [Rule::R001, Rule::R002]
  end
  
  subject do
    Checkout.new(rules)
  end

  after :each do
    puts subject.items.map(&:value)
  end
  
  it 'tests scenario 1' do
    subject.scan(Product::P001)
    subject.scan(Product::P002)
    subject.scan(Product::P003)
    expect(subject.total).to eq 6678
  end

  it 'tests scenario 2' do
    subject.scan(Product::P001)
    subject.scan(Product::P003)
    subject.scan(Product::P001)
    expect(subject.total).to eq 3695
  end

  it 'tests scenario 3' do
    subject.scan(Product::P001)
    subject.scan(Product::P002)
    subject.scan(Product::P001)
    subject.scan(Product::P003)
    expect(subject.total).to eq 7376
  end
end
